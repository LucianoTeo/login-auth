import React from 'react';

const Loading = () => {

    return (
        <section className="centered full-height">
            <i className="fas fa-spinner fa-spin" />
        </section>
    )

}

export default Loading;