import { api_url } from '../services';

export const getToken = () => localStorage.getItem('token');

export const removeToken = () => localStorage.removeItem('token');

export const removeUser = () => localStorage.removeItem('user');

export const isAuthenticated = () => {
    if (getToken()) {
        return true;
    } else {
        return false;
    }
};

export const auth = async (user, password) => {

    try {
        const response = await api_url.post('user/auth', { user, password });
        const { data } = response;

        // save localstorage
        localStorage.setItem('user', data.user);
        localStorage.setItem('token', data.token);

        // redirect
        window.location.href = '/dashboard';
        
        return data;
    }
    catch (err) {
        const { error } = err.response.data;
        return { error };
    }

};

export const logout = () => {
    removeToken();
    removeUser();
    window.location.href = '/';
}