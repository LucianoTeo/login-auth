import React from 'react';
import { logout } from '../utils/auth';

const Dashboard = () => {

    return (
        <section className="centered full-height">
            <button
                className="btn-primary"
                onClick={logout}
            >
                logout
            </button>
        </section>
    )
    
}

export default Dashboard;