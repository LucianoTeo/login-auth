const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();

const bodyParser = require('body-parser');
const cors = require('cors');

// set express
const app = express();

// body parser
app.use(bodyParser.json());

// cors
app.use(cors({
	origin: '*',
	methods: ['GET', 'POST']
}));


// connect database 
mongoose.connect(process.env.db_url, { useNewUrlParser: true });
mongoose.connection.on('connected', () => {
	console.log('connected in database');
});
mongoose.set('useCreateIndex', true);

// import and set routes
const index = require('./routes');
const users_routes = require('./routes/users');

app.use('/', index);
app.use('/users', users_routes);


// set port
const PORT = process.env.PORT || 5000
app.listen(PORT);

