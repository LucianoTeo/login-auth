import React, { useEffect } from 'react';
import LoginForm from '../Components/LoginForm';
import { isAuthenticated } from '../utils/auth';

const Login = () => {
    
    useEffect(() => {
        if (isAuthenticated()) {
            window.location.href = '/dashboard'
        }
    })

    function userRegistry() {
        console.log('registry');
    }

    return (

        !isAuthenticated() && (
            <section className="container full-height centered">
                <div className="card shadow-5">
                    <div className="card-body">
                        <LoginForm />
                        <div className="display-flex">
                            <p>Do not have an account ?</p>
                            <button
                                onClick={userRegistry}
                                className="btn-transparent ml-2">
                                <strong>register</strong>
                            </button>
                        </div>
                    </div>
                </div>
            </section>
        )

    )

}

export default Login;