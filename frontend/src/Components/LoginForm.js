import React, { useState } from 'react';

import Toast from '../Components/Toast';

import { auth } from '../utils/auth';

const LoginForm = () => {
    const [user, setUser] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [toast, setToast] = useState('hide');
    const [loading, setLoading] = useState(false);
    const [rememberLogin, setRememberLogin] = useState(false);


    async function loginIn(e) {
        e.preventDefault();

        setLoading(true);

        if (!user || !password) {
            setError('fill in the fields');
            setLoading(false);
            
            setToast('show');
            setTimeout(() => {
                setToast('hide');
                setError('');
            }, 2000);
            
            return;

        }

        // call auth
        const checkAuth = await auth(user, password);
    
        // if any error
        if (checkAuth.error) {
            setError(checkAuth.error);
            setLoading(false);

            setToast('show');
            setTimeout(() => {
                setToast('hide');
            }, 2000);
        }

    }

    function handleUser(event) {
        const { value } = event.target;
        if (!value) {
            setError('');
            setToast('hide');
        }
        setUser(value);
    }

    function handlePassword(event) {
        const { value } = event.target;
        if (!value) {
            setError('');
            setToast('hide');
        }
        setPassword(value);
    }

    return (
        <>
            <Toast
                isOpen={toast}
                text={error}
                type="danger"
            />
            <form
                className="display-grid mb-5"
                onSubmit={(event) => loginIn(event)
                }>
                <input
                    type="text"
                    placeholder="user"
                    onChange={(event) => handleUser(event)}
                    className={`mb-5 ${error && 'input-error'}`}
                />
                <input
                    type="password"
                    placeholder="password"
                    onChange={(event) => handlePassword(event)}
                    className={`mb-5 ${error && 'input-error'}`}
                    autoComplete="on"
                />

                <div className="form-group ">
                    <div className="form-group-button">
                        <button className={`btn-primary ${loading && 'btn-loading'}`}>
                            {
                                loading ? 'signing...' : 'Sign in'
                            }
                        </button>
                    </div>
                    <div className="form-group-check">
                        <label className="custom-checkbox">
                            <input
                                type="checkbox"
                                id="checkID"
                                checked={rememberLogin}
                                onChange={(e) => setRememberLogin(!rememberLogin)}
                            />
                            <label htmlFor="checkID" className="mr-4 hidden-xs">
                                remember me
                            </label>
                            <span className="checkmark"></span>
                        </label>
                    </div>
                </div>

            </form>
        </>
    );
}

export default LoginForm;