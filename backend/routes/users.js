const express = require('express');
const router = express.Router();

const USERS_MODEL = require('../models/userModel');

// get all
router.get('/', async (req, res) => {
    try {
        const users_all = await USERS_MODEL.find({});
        return res.send({ users: users_all });
    }
    catch (err) {
        return res.send({ error: 'failed in get users' });
    }
});

// create one
router.post('/create', async (req, res) => {
    const { user } = req.body;

    try {
        if (await USERS_MODEL.findOne({ user })) {
            return res.status(400).send({ error: 'User already exist' });
        }

        const user_create = await USERS_MODEL.create(req.body);

        // set passowrd undefined
        user_create.password = undefined;

        return res.status(201).send({ user: user_create })
    }
    catch (error) {
        return res.status(500).send({ error: 'failed in create a user' });
    }
});

// authentication
router.post('/auth', async (req, res) => {
    const { user, password } = req.body;

    try {
        const user_find = await USERS_MODEL.findOne({ user }).select('+password');

        if (!user_find) {
            return res.status(401).send({ error: 'user not registried' });
        }

        if (password !== user_find.password) {
            return res.status(401).send({ error: 'invalid credentials' })
        }

        // set passowrd undefined
        user_find.password = undefined;

        return res.send({ user: user_find, token: 'faketoken' })
    }
    catch (err) {
        return res.status(500).send({ error: 'failed in authenticated a user' })
    }

});



module.exports = router;