import React, { Suspense } from 'react';
import "./styles/css/index.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Loading from './Components/Loading';

import { isAuthenticated } from './utils/auth';

const Login = React.lazy(() => import('./Pages/Login'));
const Dashboard = React.lazy(() => import('./Pages/Dashboard'));


const PrivateRoute = ({ component: Component, ...rest }) => (

  <Route
    {...rest}
    render={props =>
      isAuthenticated()
        ? <Component {...props} />
        : <Redirect to="/" />
    }
  />

)

const App = () => {

  return (

    <Suspense fallback={<Loading />}>
      <Router>
        <Switch>
          <Route path="/login" component={() => <Login />} />
          <PrivateRoute path="/dashboard" component={() => <Dashboard />} />
          <Redirect from="/" to="/login" />
          <Route path="*" component={() => <h1>Page not found</h1>} />
        </Switch>
      </Router>
    </Suspense>

  );

}

export default App;
