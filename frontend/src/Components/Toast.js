import React from 'react';

// import { Container } from './styles';

const Toast = ({ text, isOpen, type }) => (
    <div className={`toast shadow-5 mb-5 ${isOpen} ${type} `}>
        <p>
            {text}
        </p>
    </div>
)
export default Toast;
